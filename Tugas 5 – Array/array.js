// Soal No. 4 (Array Multidimensi)
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
function dataHandling (input) {
for (var i = 0; i < input.length; i++) {
console.log('Nomor ID : ' + input[i][0]);
console.log('Nama Lengkap : ' + input[i][1]);
console.log('TTL : ' + input[i][2]);
console.log('Hobby : ' + input[i][3]);
console.log('-------------------------------');
}
}
dataHandling(input);

// Soal No. 6 (Metode Array)
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandling2(input) {
  input.splice(1, 1, "Roman Alamsyah Elsharawy");
  input.splice(4, 1, "Pria");
  input.splice(5, 0, "SMA Internasional Metro");
  console.log(input);
  var angkaBulan = input[3].split('/');
  var namaBulan;
  switch (angkaBulan[1]) {
    case '01':
      namaBulan = 'Januari';
      break;
    case '02':
      namaBulan = 'Februari';
      break;
    case '03':
      namaBulan = 'Maret';
      break;
    case '04':
      namaBulan = 'April';
      break;
    case '05':
      namaBulan = 'Mei';
      break;
    case '06':
      namaBulan = 'Juni';
      break;
    case '07':
      namaBulan = 'Juli';
      break;
    case '08':
      namaBulan = 'Agustus';
      break;
    case '09':
      namaBulan = 'September';
      break;
    case '10':
      namaBulan = 'Oktober';
      break;
    case '11':
      namaBulan = 'November';
      break;
    case '12':
      namaBulan = 'Desember';
      break;
    default:
  }
  console.log(namaBulan);
  angkaBulan.sort(function(value1, value2) { return value1 < value2});
  console.log(angkaBulan);
  var tanggal = angkaBulan.join("-");
  console.log(tanggal);
  input.splice(1,1, input[1].split("").slice(0, 14).join(''));
  console.log(input);
}
dataHandling2(input);



 

// Soal No. 5 (Balik Kata)

 function balikKata(str) {
  var currentString = str;
  var newString = '';
 for (let i = str.length - 1; i >= 0; i--) {
   newString = newString + currentString[i];
  }
  
  return newString;
 }
 console.log(balikKata('SanberCode'));

//  Soal No. 1 (Range) 
 function range (start, finish) {
   if(!start || !finish) return -1;
   let arr = [];
   if(start > finish) {
     for(let i = start; i >= finish; i--) {
       arr.push(i);
     }
   } else {
     for(let i = start; i<= finish; i++) {
       arr.push(i);
     }
   }
   return arr
 }

 console.log(range(7,1))

//  Soal No. 2 (Range with Step)

var rangeWithStep = function(start, end, step) {
  var rangeWithStep = [];
  var typeofStart = typeof start;
  var typeofEnd = typeof end;

  
  if (step === 0) {
      throw TypeError("Step cannot be zero.");
  }

  if (typeofStart == "undefined" || typeofEnd == "undefined") {
      throw TypeError("Must pass start and end arguments.");
  } else if (typeofStart != typeofEnd) {
      throw TypeError("Start and end arguments must be of same type.");
  }

  typeof step == "undefined" && (step = 1);

  if (end < start) {
      step = -step;
  }

  if (typeofStart == "number") {

      while (step > 0 ? end >= start : end <= start) {
        rangeWithStep.push(start);
          start += step;
      }

  } else if (typeofStart == "string") {

      if (start.length != 1 || end.length != 1) {
          throw TypeError("Only strings with one character are supported.");
      }

      start = start.charCodeAt(0);
      end = end.charCodeAt(0);

      while (step > 0 ? end >= start : end <= start) {
        rangeWithStep.push(String.fromCharCode(start));
          start += step;
      }

  } else {
      throw TypeError("Only string and number types are supported");
  }

  return rangeWithStep;

}

console.log(rangeWithStep(11, 23, 3))


// Soal No. 3 (Sum of Range)


function rangeSums(start, end, step) {
  var numbers = [];
  if(step === undefined)
    step = 1;
  if(step>0)
    for(var i=start; i<=end; i+=step) {
      numbers.push(i);
    }
  else
    for(var i=end; i>=start; i+=step) {
      numbers.push(i);
    }
  return numbers;
}

function sum(array) {
  if(array.length === 0)
    return 0;
  return array.pop() + sum(array);
}

console.log(sum(rangeSums(5,50,2)));



// No. 1 Looping While 

console.log("LOOPING PERTAMA");
var loop1 = 2;
while(loop1 <= 20) {
  console.log(loop1 +"- I love Coding" );
  loop1 += 2;
}

console.log ("\n");




console.log ("LOOPING KEDUA")
var loop2 = 20;
while (loop2>=2) {
  console.log (loop2 + " - I will become a mobile developer")
  loop2 -=2;
}

console.log ("\n");

// No. 2 Looping menggunakan for

var pertama = " - Santai"
var kedua = " - berkualitas"
var ketiga = " - I love coding"

for (i = 1; i<=20; i++) {
  if ( i%2 !=1) {
    console.log(i + kedua)
  } else if ( i % 3 ==0) {
    console.log (i + ketiga)
  } else {
    console.log ( i + pertama)
  }
}


console.log ("\n");

// No. 3 Membuat Persegi Panjang 
var i = 1;
var j = 1;
var panjang = 8;
var lebar = 4;
var pagar = " ";

while ( j <= lebar ) {
  while ( i <= panjang) {
    pagar += "#";
    i++;
  }
  console.log(pagar);
  pagar = " ";
  i =1;
  j++;
}


console.log ("\n");

// No. 4 Membuat Tangga 

var i = 1;
var j = 1;
var alas = 7;
var tinggi = 7;
var pagar = "";


for ( i = 1; i <= tinggi; i++) {
  for ( j=1; j<=i; j++){
    pagar +="#";
  }
  console.log(pagar)
  pagar = "";
}

console.log ("\n");

// No. 5 Membuat Papan Catur

var i = 1;
var j = 1;
var panjang = 8;
var lebar = 8;
var papan = "";

for ( j = 1; j <= lebar; j++) {
  if (j%2 ==1) {
    for ( i=1 ; i<= panjang; i++) {
      if (i% 2 == 1) {
        papan +=" "
      } else {
        papan +="#"
      }
    }
  } else {
    for ( i= 1; i<= panjang; i++){
      if(i%2 ==1){
        papan +="#"
      } else {
        papan +=" "
      }
    }
  }
  console.log (papan);
  papan = "";
}

// No. 1 
function teriak() {
    console.log("Halo Sanbers!");
  }
  
  console.log(teriak());

console.log("\n")

// No. 2

  function kalikan(a,b){
    hasilKali = a * b;
    return hasilKali;
}

// memanggil fungsi
var num1 = 12;
var num2 = 4;
var hasilKali = kalikan(num1, num2);

console.log(hasilKali); //-> 48


console.log("\n")
// No. 3 
function introduce(name_param, age_param, address_param, hobby_param) {
    console.log("Nama saya " + name_param + ", umur saya " + age_param  + " tahun " +", alamat saya di " + address_param + ", dan saya punya hobby yaitu " + hobby_param);
  }
  
  var name = "Agus";
  var age = 30;
  var address = "Jln. Malioboro, Yogjakarta";
  var hobby = "gaming";
  
  var gabung_kalimat = introduce(name,age,address,hobby);
  console.log(gabung_kalimat); // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogjakarta, dan saya punya hobby yaitu gaming!"